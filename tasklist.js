window.onload = function(){
    const elmRefs = getAllRefElemets();
    initList(elmRefs);
    subscribeOnEvent(elmRefs);
}

function Task(name, description){
    this.name = name;
    this.description = description;
}

function Render(elmRef,data,renderFunc){
    this.elmRef = elmRef;
    this.data = data;
    this.renderFunc = renderFunc;
}

Render.prototype = {
    render: function(){
        this.renderFunc(this.elmRef,this.data);
    },
    renderComplitedList: function(){
        this.renderFunc(this.elmRef,this.data, true);
    },
    renderComplitTask(){
        this.renderFunc(this.elmRef,this.data, true);
    }
}

const Storage = {
    get: function(key){
        return localStorage.getItem((key));
    },

    has: function(key){
        return localStorage.getItem(key) !== null;
    },

    set: function(key,val){
        const data = this.has(key) ? this.get(key)+'-&-'+JSON.stringify(val) : JSON.stringify(val);
        localStorage.setItem(key,data);
        return this;
    },

    clear: function(key,val){
        localStorage.clear();
        return this;
    },

    remove: function(key) {
        localStorage.removeItem(key);
        return this;
    },

    serialize: function (key) {
        if(!this.has(key)) return;
        return this.get(key).split('-&-');
    },

    getLenght: function(key){
        if(this.serialize(key)) return this.serialize(key).length;
    }
}

function initList(elmRefs){
    new Render(
        elmRefs.taskList,
        Storage.serialize('tasks'),
        renderFunctions.renderList
    ).render();
    new Render(
        elmRefs.complitTaskList,
        Storage.serialize('complited-tasks'),
        renderFunctions.renderList
    ).renderComplitedList();
    elmRefs.count.textContent = Storage.getLenght('tasks');
    elmRefs.complitedCount.textContent = Storage.getLenght('complited-tasks');
}

const renderFunctions = {
    renderList: function (target,data,comlited = false) {
        if (!data) return;
        if (!data.join) return;
        const tasksContainer = document.createDocumentFragment();
        data.forEach((val, index)=>{
            const data = JSON.parse(val);
            const taskContainer = document.createElement('div');
            const h3 =  document.createElement('h3');
            const p =  document.createElement('p');
            const add =  document.createElement('div');
            if(!comlited) {
                add.classList.add('add');
                add.classList.add('add_icon')
                add.dataset.index = index;
                taskContainer.appendChild(add);
            }
            taskContainer.classList.add('container__task_list_task');
            h3.textContent = data.name;
            p.textContent = data.description;
         
            taskContainer.appendChild(h3);
            taskContainer.appendChild(p);
            tasksContainer.appendChild(taskContainer);
        });
        target.appendChild(tasksContainer);
    },
    renderTask: function (target,data, comlited = false) {
        const taskContainer = document.createElement('div');
        const h3 =  document.createElement('h3');
        const p =  document.createElement('p');
        const add =  document.createElement('div');
        if(!comlited){
            add.classList.add('add');
            add.classList.add('add_icon')
            add.dataset.index = target.children.length;
            taskContainer.appendChild(add);
        }
        taskContainer.classList.add('container__task_list_task');
        h3.textContent = data.name;
        p.textContent = data.description;
        taskContainer.appendChild(h3);
        taskContainer.appendChild(p);
        target.appendChild(taskContainer);
    }
}

function subscribeOnEvent(elmRefs){
    elmRefs.addTask.addEventListener('click',function (){
        elmRefs.overlay.style.display = 'flex';
    });
    elmRefs.clear.addEventListener('click',function(){
        Storage.remove('tasks')
        elmRefs.taskList.innerText = "";
        elmRefs.count.textContent = Storage.getLenght('tasks');
    });
    elmRefs.taskList.addEventListener('click', (event)=>{
        const index = event.target.dataset.index;
        if(index){
            const tasks = Storage.serialize('tasks');
            const task = JSON.parse(tasks[+index]);
            Storage.remove('tasks')
            tasks.splice(+index,1)
            tasks.forEach(val => Storage.set('tasks', JSON.parse(val)))
            elmRefs.taskList.children[+index].remove();
            [].slice.call(elmRefs.taskList.children, +index).forEach(elm => elm.children[0].dataset.index -= 1)
            elmRefs.count.textContent = Storage.getLenght('tasks');
            elmRefs.search.value = '';
            elmRefs.search.dispatchEvent(new Event('input'));
            new Render(elmRefs.complitTaskList, task, renderFunctions.renderTask).renderComplitTask();
            Storage.set('complited-tasks',task);
            elmRefs.complitedCount.textContent = Storage.getLenght('complited-tasks');
        }
        event.preventDefault();
    })

    elmRefs.saveTask.addEventListener('click',function(){
        if(elmRefs.newTaskInput.children[0].value){
            elmRefs.newTaskInput.children[0].style.borderBottomColor = "orange"; //TODO add validators class
            elmRefs.overlay.style.display = 'none';
            const data = {
                name:elmRefs.newTaskInput.children[0].value,
                description: elmRefs.newTaskInput.children[1].value
            };

            const promis = new Promise(res=>{
                setTimeout(()=>{
                    res(data)
                },2000)
            })

            promis.then(res=>{
                Storage.set('tasks',res);
                elmRefs.count.textContent = Storage.getLenght('tasks');
                new Render(elmRefs.taskList, data, renderFunctions.renderTask).render();
            })

        }else{
            elmRefs.newTaskInput.children[0].style.borderBottomColor = "red";
        }
    });

    elmRefs.search.addEventListener('input', (event) => {
        find(elmRefs.complitTaskList, event.target.value);
    });
}

function getAllRefElemets(){
    return {
        clear: document.getElementById('clear'),
        addTask: document.getElementById('add_task'),
        taskList: document.getElementById('task_list'),
        count: document.getElementById('count_task'),
        overlay: document.getElementById('overlay'),
        saveTask: document.getElementById('save_task'),
        newTaskInput: document.getElementById('new-task-input'),
        complitTaskList: document.getElementById('comlite-tasks-list'),
        complitedCount: document.getElementById('comlite_count_task'),
        search: document.getElementById('search'),
    }
}

function find(elmRef, input){
    elmRef.innerHTML = '';
    const search = input
    const tasks = Storage.serialize('complited-tasks').map(val => JSON.parse(val));
    if(!search.length){
        tasks.forEach(task => new Render(elmRef, task, renderFunctions.renderTask).renderComplitTask())
    } else {
        const find = tasks
            .filter(val => (
                val.name.toLowerCase().includes(search.toLowerCase()) || 
                val.description.toLowerCase().includes(search.toLowerCase())
            )
        );
        find.forEach(task => new Render(elmRef, task, renderFunctions.renderTask).renderComplitTask())
    }
}







