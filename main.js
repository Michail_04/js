const table = Array(16);
const imgPath ='./img/';
const containerRef = document.getElementById("container");
containerRef.addEventListener('click',click)
let clickCounter = 0;
let targetState = 0;
let check = false;
const clickElmRef = {
    first: null,
    second: null
}
for(let i = 1; i<9;i++){
    for(let j =0; j<2;){
    const index = Math.floor(Math.random() * (16 - 0)) + 0;
        if(!table[index]){
        const elm = document.createElement("div");
        const img = document.createElement("img");
        img.setAttribute("src","img/"+i+".jpg");
        elm.appendChild(img);
        elm.setAttribute("data-id",i);
        table[index] = elm;
        j++;
        }
    }
}
table.forEach((val,key)=>{
	val.setAttribute("id",key);
    containerRef.appendChild(val);
})
function click(e){
    if(e.target.id != "container" && !e.target.dataset.delete && !check){
        clickCounter++;
        e.target.firstChild.style.transform = "scale(1)";
        if(clickCounter == 1){
            clickElmRef.first = e.target
        }else if(clickElmRef.first != e.target){
			check = true;
            clickElmRef.second = e.target;
            compare();
        }else{
			clickCounter--;
		}
    }
	
} 

function compare(){
    const first = clickElmRef.first;
    const second = clickElmRef.second;
    if(first.dataset.id == second.dataset.id){
		first.firstChild.parentElement.style.cursor = "default";
		second.firstChild.parentElement.style.cursor = "default";
        setTimeout(()=>{
            first.firstChild.style.transform = "scale(0)";
            second.firstChild.style.transform = "scale(0)";
            first.dataset.delete = true;
            second.dataset.delete = true;
            first.style.background = "rgba(0,0,0,.5)";
            second.style.background = "rgba(0,0,0,.5)";
            clickCounter = 0;
			check = false;
        },500);
    }else{
        setTimeout(()=>{
            first.firstChild.style.transform = "scale(0)";
            second.firstChild.style.transform = "scale(0)";
            clickCounter = 0;
			check = false;
        },1500);
    }
    
}



(function(){
    const temp = console.log;
    console.log = function(){
        temp.apply(null,arguments);
        Array.from(arguments).forEach(val=>{
            localStorage.setItem(val,val);
        })
    }
})()