const gulp        = require('gulp');
const browserSync = require('browser-sync').create();

gulp.task('serve', function() {

    browserSync.init({
        server: "./"
    });

    gulp.watch(["css/*.css","*.html","*.js"]).on('change', browserSync.reload);
});