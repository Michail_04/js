
window.onload = function(){
    new Calc().run();
}

function Calc(){
    this.tempCalc = null;
    this.target = null;
    this.temp = null;
    this.result = 0;
    this.number = 0;
    this.checked = false;
    this.operators = operators;
    this.listners = this.bindContext(listners);
    this.helpers = this.bindContext(helpers);
    this.refs = this.helpers.getRefs();
    this.refs.input.textContent = this.result;
}

Calc.prototype.run = function(){
    this.subscribeListners();
}

Calc.prototype.subscribeListners = function(){
    this.refs.main.addEventListener('click', this.listners.operators.bind(this))
    this.refs.main.addEventListener('click', this.listners.num.bind(this))
}

Calc.prototype.bindContext = function(obj, param = null){
    return Object.keys(obj).reduce((newObj, prop) => {
        newObj[prop] =  param ? obj[prop].bind(this, param) : obj[prop].bind(this);
        return newObj;
    }, {});
}

const operators = (function(){
    const sum = function(a , b){
     return a ? a + b : b;   
    }
    const divide = function(a , b){
        return a ? a / b : b;    
    }
    const multiply = function(a , b){
        return a ? a * b : b;   
    }
    const difference = function(a , b){
        return a ? a - b : b;   
    }
    const calc = function(callback, number){
        if(this.checked){
            this.result = callback(number);
            this.refs.input.textContent = this.result;
        }   
    }

    return {
        calc,
        sum,
        divide,
        multiply,
        difference
    };
})();

const listners = (function(){
   const operators = function(event){
    if (this.helpers.hasClass('operator', event.target)) { 
       this.helpers.calc(event.target.textContent)
    }
   };
   const num = function(event){
        if (this.helpers.hasClass('number', event.target)) {
            this.number = +event.target.textContent;
            if (this.checked){
                if ( +this.refs.input.textContent === 0) {
                    this.refs.input.textContent = this.number ? this.number : 0;
                } else{
                    this.refs.input.textContent += this.number;
                }
            } else {
                this.refs.input.textContent = this.number;
            }
            this.checked = true;
        } else if(this.helpers.hasClass('clear', event.target)){
            this.result = 0;
            this.refs.input.textContent = 0;
            this.refs.temp.textContent = '';
        }
       
    };

   return {
     operators,
     num
   };
})()


const helpers = (function(){
    const hasClass = function hasClass(calss, target){
        return target.classList.contains(calss);
    };
    const addTemp = function(value, operator){
        const temp = this.refs.temp.textContent;
        if (!temp) { 
            this.refs.temp.textContent = value + ' ' +operator;
        } else if(!this.checked){
            this.refs.temp.textContent = this.refs.temp.textContent.slice(0,-2) + ' ' + operator;
        } else {
            this.refs.temp.textContent =  this.refs.temp.textContent + ' ' + value + ' ' + operator + ' ';
        }
    };
    const getRefs = function(){
        return {
            main: document.querySelector('.main'),
            input: document.querySelector('.input'),
            temp: document.querySelector('.temp'),
        };
    };

    const calc = function(target, temp = null){
        if(target !== '='){
            temp ? this.refs.temp.textContent = '' : this.helpers.addTemp(this.refs.input.textContent, target);
        } else if( this.tempCalc){
            this.checked = true;
            this.tempCalc(+this.refs.input.textContent);
            this.tempCalc = null;
            this.refs.temp.textContent = '';
            this.checked = false;
            return;
        }
        if( this.tempCalc){
            this.tempCalc(+this.refs.input.textContent);
        }
            switch(target){
                case '+':
                    this.target = '+'; 
                    this.tempCalc = this.operators.calc.bind(this, this.operators.sum.bind(this, +this.refs.input.textContent))
                break;
                case '-':
                    this.target = '-';
                    this.tempCalc = this.operators.calc.bind(this, this.operators.difference.bind(this, +this.refs.input.textContent))
                break;
                case '*':
                    this.target = '*';                  
                    this.tempCalc = this.operators.calc.bind(this, this.operators.multiply.bind(this, +this.refs.input.textContent))
                break;
                case '/':
                    this.target = '/';                  
                    this.tempCalc = this.operators.calc.bind(this, this.operators.divide.bind(this, +this.refs.input.textContent))
                break;
            };
            this.checked = false
    };
    return {
        calc,
        getRefs,
        hasClass,
        addTemp
    };
})()







